const api11 = 'http://www.omdbapi.com/?apikey=bb2c3cc6'
const api_id11 = 'http://www.omdbapi.com/?i='
const key11 = '&apikey=bb2c3cc6'
const api01 = 'https://api.themoviedb.org/3/search/movie?api_key=0fb6e01115f262c0d47f6ec85d65e9b7'
const api_person01 = 'https://api.themoviedb.org/3/search/person?api_key=0fb6e01115f262c0d47f6ec85d65e9b7'
const key01 = '?api_key=0fb6e01115f262c0d47f6ec85d65e9b7'
const api_id01 = 'https://api.themoviedb.org/3/movie/'

async function getTrending()
{
    var response = await fetch('https://api.themoviedb.org/3/movie/top_rated?api_key=0fb6e01115f262c0d47f6ec85d65e9b7&language=en-US');
    var rs = await response.json();
    var list_key1 = getID01(rs.results);
    
    $('#main').empty();
    $('#firtview').empty();
    for (var i = 0; i < list_key1.length; i++)
        {   //..............lay data id phim chuyen vo mang...........
            var K = list_key1[i];
            reqString = `${api_id01}${K}${key01}&language=en-US`;
            response = await fetch(reqString);
            rs = await response.json();      
            //........................ data ra man hinh............. 
            if(rs.imdb_id!=null && rs.imdb_id!="")
            {
                reqString1 = `${api_id11}${rs.imdb_id}${key11}`;
                response1 = await fetch(reqString1);
                rs1 = await response1.json();
                fillmovie01(rs1);
                //........................ review.......................
                reqStringRV = `${api_id01}${K}/reviews${key01}`;
                responseRV = await fetch(reqStringRV);
                rsRV = await responseRV.json();
                m=`${rs1.imdbID}`;
                for (const i of rsRV.results)
                {
                    fillreview01(i,m);
                }
            }

        }



}
function getID01 (ms) // tìm phim theo tên phim
{
   
    const list_key = [];
    for (const m of ms)
    {
        list_key.push(m.id);
    }
    return list_key;
}
async function fillreview01(ms,k)
{
    $('#reviewmovie'+k).append(`
        <p class="text-center font-weight-bold  ">Reviews</p>
        <p class="card-title font-weight-bold">${ms.author}</p>
        <p>Caption:</p>
        <p>${ms.content}</p>
        <hr  width="80%" size="5px" align="center" color="black" />
    `);
}
async function fillmovie01 (ms)
{
    
    
        $('#main').append(`
        
            <div class="col-md-3">
                <div class="card shadow h-100">
                <button type="button" data-toggle="modal" data-target="#myModal${ms.imdbID}">
                    <img class="card-img-top" src="${ms.Poster}" alt="${ms.Title}  >
                </button>    
                    <div class="card-body">
                        <h5 class="card-title">${ms.Title}</h5>
                        <p class="card-text">${ms.Plot}</p></br>
                        <p class="card-text"> <b>Genre: </b> ${ms.Genre}</p>
                        <p class="card-text"><b>Rating: </b> ${ms.imdbRating}%</p>
                        <p class="card-text"><b>Runtime: </b> ${ms.Runtime}</p>                               
                    </div>
                </div> 
                
            </div>
        
        <div class="modal fade" id="myModal${ms.imdbID}" role="dialog">
            <div class="modal-dialog modal-lg">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                       
                        <img id= "left" class=" py-2" src="${ms.Poster}" alt="${ms.Title}" width="100px" >
                        <button type="button" class="close" data-dismiss="modal">&times;</button>  
                    </div>
                    <div class="modal-body  py-2">
                        <h2 id= "mid" class="card-title  py-2">${ms.Title}</h5></br>
                        <p class="card-text">${ms.Plot}</p></br>
                        <p class="card-text"> <b>Genre: </b> ${ms.Genre}</p>
                        <p class="card-text"><b>Rating: </b> ${ms.imdbRating}%</p>
                        <p class="card-text"><b>Runtime: </b> ${ms.Runtime}</p>
                        <p class="card-text"><b> Country:</b> ${ms.Country}</p>
                        <p class="card-text"><b>Director: </b> ${ms.Director}</p> 
                        <p class="card-text"><b>Language: </b> ${ms.Language}</p>
                        <p class="card-text"><b>Production: </b> ${ms.Production}</p>
                        <p class="card-text"><b>Writer: </b> ${ms.Writer}</p> 
                        <p class="card-text"><b>Actors: </b> ${ms.Actors}</p> 
                        <p class="card-text"><b>year: </b> ${ms.Released}</p> 
                        <button type="button" data-toggle="modal" data-target="#myModal${ms.imdbID}RV">See Reviews...</button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>

        <div class="modal fade" id="myModal${ms.imdbID}RV" role="dialog">
            <div class="modal-dialog modal-lg">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                       
                        <img id= "left" class=" py-2" src="${ms.Poster}" alt="${ms.Title}" width="100px" >
                        <button type="button" class="close" data-dismiss="modal">&times;</button>  
                    </div>
                    <div id="reviewmovie${ms.imdbID}" class="modal-body  py-2">
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>
        `);
        


}
function loading()
{
    $('#main').empty();
    $('#main').append(`
    <div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
    </div>`
    );
    
}