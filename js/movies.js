const api1 = 'http://www.omdbapi.com/?apikey=bb2c3cc6'
const api_id1 = 'http://www.omdbapi.com/?i='
const key1 = '&apikey=bb2c3cc6'
const api = 'https://api.themoviedb.org/3/search/movie?api_key=0fb6e01115f262c0d47f6ec85d65e9b7'
const api_person = 'https://api.themoviedb.org/3/search/person?api_key=0fb6e01115f262c0d47f6ec85d65e9b7'
const key = '?api_key=0fb6e01115f262c0d47f6ec85d65e9b7'
const api_id = 'https://api.themoviedb.org/3/movie/'

async function evtSupmit(e)
{
    e.preventDefault();
    //................
        
    //.............

    const strSearch = $('form input').val();
    
    var reqString = `${api}&query=${strSearch}`;
    loading();
    
    var response = await fetch(reqString);
    var rs = await response.json();
    
    if(rs.results.length==0)
    {   
        
        reqString = `${api_person}&query=${strSearch}`;
        
        loading();
    
        response = await fetch(reqString);
        rs = await response.json();
        const list_key = getID1(rs.results);
        $('#main').empty();
        for (const K of list_key)
        {   //..............lay data id phim chuyen vo mang...........
            reqString = `${api_id}${K}${key}&language=en-US`;
            response = await fetch(reqString);
            rs = await response.json();      
            //........................do data ra man hinh............. 
            if(rs.imdb_id!=null && rs.imdb_id!="")
            {
                reqString1 = `${api_id1}${rs.imdb_id}${key1}`;
                response1 = await fetch(reqString1);
                rs1 = await response1.json();
                fillmovie(rs1);
                //........................do review.......................
                reqStringRV = `${api_id}${K}/reviews${key}`;
                responseRV = await fetch(reqStringRV);
                rsRV = await responseRV.json();
                m=`${rs1.imdbID}`;
                for (const i of rsRV.results)
                {
                    fillreview(i,m);
                }
            }
            

        }
    }
    else
    {
        const list_key = getID(rs.results);
        $('#main').empty();
        for (const K of list_key)
        {   //..............lay data id phim chuyen vo mang...........
            reqString = `${api_id}${K}${key}&language=en-US`;
            response = await fetch(reqString);
            rs = await response.json();      
            //........................do data ra man hinh............. 
            if(rs.imdb_id!=null && rs.imdb_id!="")
            {
                reqString1 = `${api_id1}${rs.imdb_id}${key1}`;
                response1 = await fetch(reqString1);
                rs1 = await response1.json();
                fillmovie(rs1);
                //........................do review.......................
                reqStringRV = `${api_id}${K}/reviews${key}`;
                responseRV = await fetch(reqStringRV);
                rsRV = await responseRV.json();
                m=`${rs1.imdbID}`;
                for (const i of rsRV.results)
                {
                    fillreview(i,m);
                }
            }

        }
    
    }
    
    
    
}


function getID1 (ms)// tìm phim theo diễn viên
{
    if (ms == null)
        {
            return []
        };
   
    var list_key = [];
    for (const m of ms)
    {
        for (const j of m.known_for)
    {
        list_key.push(j.id);
       
    }
    
    }
    return list_key;
    
}
function getID (ms) // tìm phim theo tên phim
{
   
    var list_key = [];
    for (const m of ms)
    {
        
        list_key.push(m.id);

    }
    return list_key;
}
function fillreview(ms,k)
{
    $('#reviewmovie'+k).append(`
        <p class="text-center font-weight-bold  ">Reviews</p>
        <p class="card-title font-weight-bold">${ms.author}</p>
        <p>Caption:</p>
        <p>${ms.content}</p>
        <hr  width="80%" size="5px" align="center" color="black" />
    `);
}
function fillmovie (ms)
{
    
    
        $('#main').append(`
        
            <div class="col-md-3">
                <div class="card shadow h-100">
                <button type="button" data-toggle="modal" data-target="#myModal${ms.imdbID}">
                    <img class="card-img-top" src="${ms.Poster}" alt="${ms.Title}  >
                </button>    
                    <div class="card-body">
                        <h5 class="card-title">${ms.Title}</h5>
                        <p class="card-text">${ms.Plot}</p></br>
                        <p class="card-text"> <b>Genre: </b> ${ms.Genre}</p>
                        <p class="card-text"><b>Rating: </b> ${ms.imdbRating}%</p>
                        <p class="card-text"><b>Runtime: </b> ${ms.Runtime}</p>                               
                    </div>
                </div> 
                
            </div>
        
        <div class="modal fade" id="myModal${ms.imdbID}" role="dialog">
            <div class="modal-dialog modal-lg">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                       
                        <img id= "left" class=" py-2" src="${ms.Poster}" alt="${ms.Title}" width="100px" >
                        <button type="button" class="close" data-dismiss="modal">&times;</button>  
                    </div>
                    <div class="modal-body  py-2">
                        <h2 id= "mid" class="card-title  py-2">${ms.Title}</h5></br>
                        <p class="card-text">${ms.Plot}</p></br>
                        <p class="card-text"> <b>Genre: </b> ${ms.Genre}</p>
                        <p class="card-text"><b>Rating: </b> ${ms.imdbRating}%</p>
                        <p class="card-text"><b>Runtime: </b> ${ms.Runtime}</p>
                        <p class="card-text"><b> Country:</b> ${ms.Country}</p>
                        <p class="card-text"><b>Director: </b> ${ms.Director}</p> 
                        <p class="card-text"><b>Language: </b> ${ms.Language}</p>
                        <p class="card-text"><b>Production: </b> ${ms.Production}</p>
                        <p class="card-text"><b>Writer: </b> ${ms.Writer}</p> 
                        <p class="card-text"><b>Actors: </b> ${ms.Actors}</p> 
                        <p class="card-text"><b>year: </b> ${ms.Released}</p> 
                        <button type="button" data-toggle="modal" data-target="#myModal${ms.imdbID}RV">See Reviews...</button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>

        <div class="modal fade" id="myModal${ms.imdbID}RV" role="dialog">
            <div class="modal-dialog modal-lg">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                       
                        <img id= "left" class=" py-2" src="${ms.Poster}" alt="${ms.Title}" width="100px" >
                        <button type="button" class="close" data-dismiss="modal">&times;</button>  
                    </div>
                    <div id="reviewmovie${ms.imdbID}" class="modal-body  py-2">
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            
            </div>
        </div>
        `);
        


}
function loading()
{
    $('#main').empty();
    $('#main').append(`
    <div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
    </div>`
    );
    
}